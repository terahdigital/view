<?php declare(strict_types=1);

namespace Terah\View;

class Text extends ViewRenderer
{
    /**
     * @param null  $data
     * @param array $viewParams
     * @return string
     */
    public function toString($data=null, array $viewParams=[]) : string
    {
        // Just output scalars
        if ( is_scalar($data) )
        {
            return (string)$data;
        }
        // Empty or associative arrays are outputted as json
        if ( is_array($data) )
        {
            reset($data);
            if ( empty($data) || is_string(key($data)) || ( isset($data[0]) && ! is_string($data[0]) ) )
            {
                return (string)json_encode($data, JSON_PRETTY_PRINT);
            }

            return implode("\n", $data) . "\n";
        }

        return (string)json_encode($data, JSON_PRETTY_PRINT);
    }
}
