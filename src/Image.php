<?php declare(strict_types=1);

namespace Terah\View;

class Image extends ViewRenderer
{
    /**
     * @param null  $data
     * @param array $viewParams
     * @return string
     */
    public function toString($data=null, array $viewParams=[]) : string
    {
        return (string)$data;
    }
}
