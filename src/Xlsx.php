<?php declare(strict_types=1);

namespace Terah\View;

use XLSXWriter;

class Xlsx extends ViewRenderer
{
    protected XLSXWriter $xlsxWriter;

    public function __construct(XLSXWriter $xlsxWriter)
    {
        $this->xlsxWriter = $xlsxWriter;
    }


    public function toString($data=null, array $viewParams=[]) : string
    {
        if ( empty($data) )
        {
            return $this->xlsxWriter->writeToString();
        }
        if ( is_string($data) )
        {
            return $data;
        }
        $data       = is_array($data) && array_key_exists('data', $data) ? $data['data'] : $data;
        $hasHeader  = array_key_exists('hasHeader', $viewParams) && $viewParams['hasHeader'] === false ? false : true;
        if ( $hasHeader )
        {
            $headerRow      = (array)$data[0];
            $header         = [];
            foreach ( $headerRow as $field => $value )
            {
                $header[ucwords(str_replace('_', ' ', strtolower($field)))] = 'string';
            }
            $this->xlsxWriter->writeSheetHeader('Sheet1', $header);
        }
        foreach ( $data as $idx => $row )
        {
            $this->xlsxWriter->writeSheetRow('Sheet1', (array)$row);
        }

        return (string)$this->xlsxWriter->writeToString();
    }
}
