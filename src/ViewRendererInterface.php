<?php declare(strict_types=1);

namespace Terah\View;

use Psr\Http\Message\ResponseInterface;

interface ViewRendererInterface
{

    public function render(ResponseInterface $response, array $viewParams=[], $data = null) : ResponseInterface;

    public function toString($data=null, array $viewParams=[]) : string;
}
