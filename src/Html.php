<?php declare(strict_types=1);

namespace Terah\View;

use Exception;
use InvalidArgumentException;

/**
 * Class Html
 *
 * @package Terah\View
 */
class Html extends ViewRenderer
{
    protected string $templatePath;

    protected array $attributes;

    protected string $defaultLayout;

    protected string $defaultLayoutPath;

    protected string $defaultExtension;

    protected string $currentExtension;

    protected string $layoutExtension;

    protected array $variables;

    public function __construct(string $templatePath='', array $attributes=[], string $defaultLayout='index', string $defaultLayoutPath='layouts', string $defaultExtension='php')
    {
        $this->templatePath         = $templatePath;
        $this->attributes           = $attributes;
        $this->defaultLayout        = $defaultLayout;
        $this->defaultLayoutPath    = $defaultLayoutPath;
        $this->defaultExtension     = $defaultExtension;
        $this->currentExtension     = $defaultExtension;
        $this->layoutExtension      = $defaultExtension;
    }


    public function toString($data=null, array $viewParams=[]) : string
    {
        $data                   = is_null($data) ? [] : $data;

        return $this->renderWithLayout($data, $viewParams);
    }


    public function renderWithLayout(array $attributes=[], array $viewParams=[]) : string
    {
        $this->variables['body_template']   = $this->renderTemplate($attributes, $viewParams);
        if ( ! empty($viewParams['tmpl_callback']) && is_callable($viewParams['tmpl_callback']) )
        {
            $this->variables['body_template'] = $viewParams['tmpl_callback']->__invoke($this->variables['body_template']);
        }
        $bufferLevel                        = ob_get_level();
        try
        {
            $layout                     = $this->defaultLayout;
            $this->currentExtension     = $this->defaultExtension;
            $this->layoutExtension      = $this->defaultExtension;
            if ( ! empty($viewParams['layout']) )
            {
                $layout                     = $viewParams['layout'];
            }
            if ( ! empty($viewParams['ext']) )
            {
                $this->currentExtension     = $viewParams['ext'];
            }
            if ( ! empty($viewParams['layout_ext']) )
            {
                $this->layoutExtension      = $viewParams['layout_ext'];
            }
            $fullPath   = $this->getTemplateFilePath($layout, 'layouts', $this->layoutExtension);
            if ( empty($fullPath) )
            {
                throw new InvalidArgumentException("Empty template file path specified");
            }
            if ( ! file_exists($fullPath) )
            {
                throw new InvalidArgumentException("Invalid template file path specified: " . $fullPath);
            }
            $output                     = $this->renderTemplateFile($fullPath);
            $this->currentExtension     = $this->defaultExtension;
            $this->layoutExtension      = $this->defaultExtension;

            return $output;
        }
        catch ( Exception $e )
        {
            while ( $bufferLevel && ob_get_level() >= $bufferLevel )
            {
                ob_end_clean();
            }
            throw $e;
        }
    }


    public function renderTemplate(array $attributes=[], array $viewParams=[]) : string
    {
        $bufferLevel    = ob_get_level();
        try
        {
            $template   = '';
            if ( array_key_exists('tmpl', $viewParams) )
            {
                $template = $viewParams['tmpl'];
            }
            if ( array_key_exists('template', $viewParams) )
            {
                $template = $viewParams['template'];
            }
            elseif ( key($viewParams) === 0 )
            {
                $template = $viewParams[0];
            }
            if ( array_key_exists('ext', $viewParams) )
            {
                $this->currentExtension = $viewParams['ext'];
            }
            if ( empty($template) )
            {
                throw new InvalidArgumentException("Empty template file specified");
            }
            $this->variables    = is_array($attributes) ? $attributes : (array)$attributes;
            $this->variables    = array_merge($this->attributes, $this->variables);
            $fullPath       = $this->getTemplateFilePath($template);
            if ( empty($fullPath) )
            {
                throw new InvalidArgumentException("Empty template file path specified");
            }
            if ( ! file_exists($fullPath) )
            {
                throw new InvalidArgumentException("Invalid template file path specified: " . $fullPath);
            }

            return $this->renderTemplateFile($fullPath);
        }
        catch( Exception $e )
        {
            while ( $bufferLevel && ob_get_level() >= $bufferLevel )
            {
                ob_end_clean();
            }
            throw $e;
        }
    }


    protected function renderTemplateFile(string $fullPath, array $attributes=[]) : string
    {
        extract($this->variables);
        extract($attributes);
        /** @noinspection PhpUnusedLocalVariableInspection */
        $view = $this;
        ob_start();
        /** @noinspection PhpIncludeInspection */
        require $fullPath;

        return ob_get_clean();
    }


    public function inc(string $relPath, array $attributes=[], string $ext='') : string
    {
        $fullPath  = $this->getTemplateFilePath($relPath, '', $ext);

        if ( empty($fullPath) )
        {
            throw new InvalidArgumentException("Empty template file path specified");
        }
        if ( ! file_exists($fullPath) )
        {
            throw new InvalidArgumentException("Invalid template file path specified: " . $fullPath);
        }

        return $this->renderTemplateFile($fullPath, $attributes);
    }


    public function esc($value) : string
    {
        return htmlspecialchars((string)$value, ENT_QUOTES | ENT_SUBSTITUTE);
    }


    public function getTemplateFilePath(string $file, string $path='', string $ext='') : string
    {
        $path           = empty($path)  ? '' : str_replace('//', '/', "{$path}/");
        $ext            = empty($ext)   ? $this->currentExtension : $ext;

        return "{$this->templatePath}{$path}{$file}.{$ext}";
    }
}
