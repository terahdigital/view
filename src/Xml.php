<?php declare(strict_types=1);

namespace Terah\View;

use SimpleXMLElement;

class Xml extends ViewRenderer
{
    public function toString($data=null, array $viewParams=[]) : string
    {
        $container  = array_key_exists('container', $viewParams) ? $viewParams['container'] : 'response';
        $xmlObj     = new SimpleXMLElement("<?xml version=\"1.0\"?><{$container}></{$container}>");

        if ( empty($data) )
        {
            return $xmlObj->asXML();
        }
        $data       = is_array($data) && array_key_exists('data', $data) ? $data['data'] : $data;
        $this->arrayToXml($data, $xmlObj);

        return $xmlObj->asXML();
    }


    protected function arrayToXml(array $data, SimpleXMLElement $xmlObj) : void
    {
        foreach ( $data as $key => $value )
        {
            $value = is_object($value) ? (array)$value : $value;
            if ( ! is_array($value) )
            {
                $xmlObj->addChild("$key", htmlspecialchars("$value"));
                continue;
            }
            if ( ! is_numeric($key) )
            {
                $subNode = $xmlObj->addChild("$key");
                $this->arrayToXml($value, $subNode);
                continue;
            }
            $subNode = $xmlObj->addChild("item{$key}");
            $this->arrayToXml($value, $subNode);
        }
    }
}
