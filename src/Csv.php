<?php declare(strict_types=1);

namespace Terah\View;

use League\Csv\Writer;


class Csv extends ViewRenderer
{

    protected Writer $csvWriter;

    protected bool $hasHeader   = true;

    public function __construct(Writer $csvWriter, bool $hasHeader=true)
    {
        $this->csvWriter = $csvWriter;
        $this->hasHeader = $hasHeader;
    }


    public function toString($data=null, array $viewParams=[]) : string
    {
        if ( is_string($data) )
        {
            return $data;
        }
        if ( empty($data) )
        {
            return '';
        }
        $hasHeader  = array_key_exists('hasHeader', $viewParams) && $viewParams['hasHeader'] === false ? false : true;
        $data       = is_array($data) && array_key_exists('data', $data) ? $data['data'] : $data;
        if ( $hasHeader )
        {
            $header     = array_map(function($underScoredWord){
                return ucwords(str_replace('_', ' ', strtolower($underScoredWord)));
            }, array_keys((array)$data[0]));
            $this->csvWriter->setNewline("\r\n");
            $this->csvWriter->insertOne($header);
        }
        foreach ( $data as $row )
        {
            $this->csvWriter->insertOne((array)$row);
        }

        return $this->csvWriter->__toString();
    }
}
