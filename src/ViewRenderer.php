<?php declare(strict_types=1);

namespace Terah\View;

use Psr\Http\Message\ResponseInterface;

/**
 * Class ViewRenderer
 */
abstract class ViewRenderer implements ViewRendererInterface
{
    public function render(ResponseInterface $response, array $viewParams=[], $data=null) : ResponseInterface
    {
        $data   = ! is_null($data) ? $data : $response->getBody() . '';
        $response->getBody()->write($this->toString($data, $viewParams));

        return $response;
    }


    abstract public function toString($data=null, array $viewParams=[]) : string;
}
