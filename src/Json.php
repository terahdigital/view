<?php declare(strict_types=1);

namespace Terah\View;

/**
 * Class Json
 *
 * @package Terah\Json
 */
class Json extends ViewRenderer
{
    /**
     * @param null  $data
     * @param array $viewParams
     * @return string
     */
    public function toString($data=null, array $viewParams=[]) : string
    {
        if ( is_null($data) || $data === '' )
        {
            return '{}';
        }
        $data       = is_array($data) && array_key_exists('data', $data) ? $data['data'] : $data;

        // Encode <, >, ', &, and " for RFC4627-compliant JSON, which may also be embedded into HTML.
        return json_encode($data, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_AMP | JSON_HEX_QUOT);
    }
}
