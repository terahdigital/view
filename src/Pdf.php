<?php declare(strict_types=1);

namespace Terah\View;

use Terah\Wkhtml\Wkhtml;

class Pdf extends ViewRenderer
{
    protected Wkhtml $wkhtml;

    protected ?ViewRendererInterface $preRenderer      = null;

    public function __construct(Wkhtml $wkhtml, ?ViewRendererInterface $preRenderer=null)
    {
        $this->wkhtml           = $wkhtml;
        $this->preRenderer      = $preRenderer;
    }


    public function toString($data=null, array $viewParams=[]) : string
    {
        if ( empty($data) || ( is_string($data) && preg_match('/^%PDF/', $data) ) )
        {
            return (string)$data;
        }
        if ( ! empty($viewParams['orientation']) )
        {
            // With landscape orientation;
            $this->wkhtml->orientation($viewParams['orientation']);
        }
        if ( ! empty($viewParams['xvfbBinary']) )
        {
            // Set the path to xvfb-run
            $this->wkhtml->xvfbBinary($viewParams['xvfbBinary']);
        }
        if ( ! empty($viewParams['xvfbArgs']) )
        {
            // Set the args for xvfb-run
            $this->wkhtml->xvfbArgs($viewParams['xvfbArgs']);
        }
        if ( ! empty($viewParams['wkhtmlArgs']) )
        {
            // Set the args for wkhtmltopdf
            $this->wkhtml->wkhtmlArgs($viewParams['wkhtmlArgs']);
        }
        if ( $this->preRenderer )
        {
            if ( ( ! isset($viewParams['preRenderer']) || $viewParams['preRenderer'] !== false ) && is_array($data) )
            {
                $data               = $this->preRenderer->toString($data, $viewParams);
            }
        }

        return $this->wkhtml->fromString($data)->toString();
    }
}
