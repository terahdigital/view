# view

[![Latest Version on Packagist][ico-version]][link-packagist]
[![Software License][ico-license]](LICENSE.md)
[![Build Status][ico-travis]][link-travis]
[![Coverage Status][ico-scrutinizer]][link-scrutinizer]
[![Quality Score][ico-code-quality]][link-code-quality]
[![Total Downloads][ico-downloads]][link-downloads]

Classes for rendering output

## Install

Via Composer

``` bash
$ composer require terah/view
```

## Usage

``` php
$skeleton = new League\Skeleton();
echo $skeleton->echoPhrase('Hello, League!');
```

## Change log

Please see [CHANGELOG](CHANGELOG.md) for more information what has changed recently.

## Testing

``` bash
$ composer test
```

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) and [CONDUCT](CONDUCT.md) for details.

## Security

If you discover any security related issues, please email terry@terah.com.au instead of using the issue tracker.

## Credits

- [Terry Cullen][https://bitbucket.org/terahdigital]
- [All Contributors][link-contributors]

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.

[ico-version]: https://img.shields.io/packagist/v/terah/view.svg?style=flat-square
[ico-license]: https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square
[ico-travis]: https://img.shields.io/travis/terah/view/master.svg?style=flat-square
[ico-scrutinizer]: https://img.shields.io/scrutinizer/coverage/g/terah/view.svg?style=flat-square
[ico-code-quality]: https://img.shields.io/scrutinizer/g/terah/view.svg?style=flat-square
[ico-downloads]: https://img.shields.io/packagist/dt/terah/view.svg?style=flat-square

[link-packagist]: https://packagist.org/packages/terah/view
[link-travis]: https://travis-ci.org/terah/view
[link-scrutinizer]: https://scrutinizer-ci.com/g/terah/view/code-structure
[link-code-quality]: https://scrutinizer-ci.com/g/terah/view
[link-downloads]: https://packagist.org/packages/terah/view
[https://bitbucket.org/terahdigital]: https://bitbucket.org/terahdigital
[link-contributors]: ../../contributors
